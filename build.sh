#!/usr/bin/env bash

BK=$(echo $@ | grep -e "--bk" -e "-b")
CL=$(echo $@ | grep -e "--cl" -e "-c")
HELP=$(echo $@ | grep -e "--help" -e "-h")

if [[ -n $HELP ]]; then
  echo -e "use --bk|-b for building the black and white bok\nuse --cl|-c for building the color book\n if none given both are being compiled"
fi

if [[ -n $BK ]]; then
  echo ifelse
  echo -e "\033[34mBuilding Book BK\033[0m\n\n"
  cd ./builddir
  ./latex-master.sh --bk

elif [[ -n $CL ]]; then
  echo elifelse
  echo -e "\033[34mBuilding Book Color\033[0m\n\n"
  cd ./builddir
  ./latex-master.sh --cl
else
  echo else
  echo -e "\033[34mBuilding Book\033[0m\n\n"
  cd ./builddir
  echo ${PWD}
  ./latex-master.sh --bk 
  ./latex-master.sh --cl
fi  

