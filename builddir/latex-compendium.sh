#!/usr/bin/env bash

FILE=$1
order=$(ls -1 | grep $FILE- | grep -v "\-all" | sort -V) 

#echo "$order"
sleep 1
for f in ${order}
do
  cat $f >> $FILE-all.tex
done

echo -e "\033[35mThe compendium file is:\033[34m $FILE-all.tex\033[0m"
