#!/usr/bin/env bash

FILEFMT=$FILE-fmt

HELP=$(echo $@ | grep -e "--help" -e "-h" )
BK=$(echo $@ | grep -e "--bk" -e "-b" )
CL=$(echo $@ | grep -e "--cl" -e "-c" )
OPTION="bk"

helpMessage() {
  echo -e "\033[32mThis Script takes the original Beowulf file from \033[1;36m Háskoli íslands\033[32m made by the Doctor \033[034m Pétur Knútsson\033[32m in 1983 close to the final printed version\n\t\033[33m(link: https://web.archive.org/web/20061220210526/http://www.hi.is/~peturk/3T/bjolfskvida.html)\033[35m\n\ttakes it and reformats it to fit into a latex book, all necesary packages and defininitions are included in basic.tex\n\tthis transformations will be applied to the given file as a environment variable \"FILE=\":\n\033[32m\t1: format it to fit the verse environment standarts, given by the verse package, same as the poetry package's poem standarts\n\t2: will take that file and create the latex chapters and tcolor boxes, \033[35m(command definitions in basic.tex)\033[32m\n\t3: will append avery single one to the file \033[33m FILENAME-fmt-all.tex \033[34m\n\t\tNOTES:\n\tchapter and tcolorbox title definitions are in the script latex-cr-auto.sh\n\tThe script title-generator generates both chapter and tcolorbox titles, even from where you want, \033[31mbe careful with reverse numbers\033[32m\nEach Script has its help and usage for further usabillity in the proyect contribution\nDefaulf text file of the bjólfskvíða translation is bj-orig, (if not exist you need to download it\ncopy from the text beggining, where is the number 1 in the 1fst column) and give it as argument or set its name to bj-orig \033[0m\n\nthe \"FILE env var\" is the path of the file containing the pre print edition of the book, though it defaults to bj-orig if no path is given also\n\n \\-\\-bk for building the black and white version\n\\-\\-cl for building the color verison"
}

if [[ -n ${HELP} ]]; then
  helpMessage
  exit 0
fi

if [[ -n $CL ]]; then
  OPTION="cl"
fi


if [[ -z ${FILE} && -e ./bj-orig  ]]; then
  echo -e "\n\033[35mNo file name was given so im using bj-orig as the default file\033[0m\n"
  FILE=bj-orig
  FILEFMT=bj-orig-fmt
else 
  echo -e "\n\033[35mNo file name was given and bj-orig does not exist, aborting\033[0m\n"
  exit 1
fi

if [[ -e $FILEFMT ]]; then 
  rm $FILEFMT*
fi

if [[ -e $FILEFMT-all ]]; then
  rm $FILEFMT-all*
fi

./latex-formater.sh $FILE

if [[  $! -eq 0 ]]; then
  ./latex-cr-auto.sh $FILEFMT 
  echo $!

  if [[  $! -eq 0 ]]; then
    ./latex-compendium.sh $FILEFMT 
    if [[ ! $! -eq 0 ]]; then
      echo -e "\n\033[35mSomething went wrong in \"latex-compendium.sh\" execution\033[0m\n"
      exit 1
    fi
  else
    echo -e "\n\033[35mSomething went wrong in \"latex-cr-auto.sh\" execution\033[0m\n"
    exit 1
  fi 
else
  echo -e "\n\033[35mSomething went wrong in \"latex-formater.sh\" execution\033[0m\n"
  exit 1 
fi

echo -e "\n\n\033[32mYeahy!!, We Got It, check for the file $FILEFMT-all.tex and basics.tex,for a SINGLE file latex book.\nfor a MODULAR book check the $FILEFMT-Numbers.tex files"

echo "\nsetting up the final file"

./final.sh $FILEFMT-all $OPTION

mv Bjólskiða-$OPTION.tex ..
cd ..

lualatex Bjólskiða-$OPTION.tex

if [[ $! -eq 0 ]]; then
  xdg-open ./Bjólskiða-$OPTION.pdf
fi


