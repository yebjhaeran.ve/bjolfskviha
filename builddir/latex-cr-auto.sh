#!/usr/bin/env bash 
FILE=$1
LINES=($(cat ./lines))
##NAMES=( "Bjólf og Danarnir" "Kafli 2" "heyrnin um grendel" $(./title-generator.sh -m 4 ) )
readarray -t NAMES < <(./title-generator.sh -m )
#TITLES=( "FORLEIKUR: BJÓLF OG DANARNIR" "Framhald 2" "GRENDEL: BYRJUNIN" $(./title-generator.sh -t 4 ))

readarray -t TITLES < <(./title-generator.sh -t )

#CMDSTR="./latex-creator.sh $FILE sCþ sTþ sNþ sLFþ sLTþ"

HELP=$(echo $@ | grep -e "--help" -e "-h" )
#if help is needed

if [[ ! -z $HELP ]]; then
  echo -e "\033[1;33mFILE=\$1,\033[1;32m anything else is hardcoded\033[0m"
else
  for INDX in $( seq 0 42 )
  do
    #the echo-s are the cuz debugging 
    #here we select the respective info for our chapter
  
    #here whe put the needed info in the command to execute
    #here we separate the lines into the requared variables
    LNF=$(echo ${LINES[$INDX]} | awk -F- '{printf $1}')
    LNT=$(echo ${LINES[$INDX]} | awk -F- '{printf $2}')
##    echo $LNF
##    echo $LNT

    #now we put all togheder
    NAME=${NAMES[$INDX]}
#    echo ${NAME}
    TITLE=${TITLES[$INDX]}
    

    # echo "$FILE" $INDX "${NAME}" "${TITLE}" "$LNF" "$LNT"
    bash ./latex-creator.sh "$FILE" $INDX "${NAME}" "${TITLE}" "$LNF" "$LNT"

  ##    echo -e $CMDSTR "\n\n"
  done

fi

exit 0
