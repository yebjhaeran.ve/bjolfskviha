#!/usr/bin/env bash

NAMESFROM=$(echo $@ | grep -e "-m")
NAMEBASE="Kapli"

TITLESFROM=$(echo $@ | grep -e "-t")
TITLEBASE="Framhald"

HELP=$(echo $@ | grep -e "-h" -e "--help")

FROM=$(echo $@ | tr " " "]" | cut -d']' -f 2 )
DEFAULT=0

generar () { # "-m"|"-t" BASENAME DEFAULTCASE "Default = 0|1" 

  if [[ $FROM == $1 ]]; then
    FROM=0
  fi

  for i in $( seq $FROM 43 ); do 
    if [[ $DEFAULT -eq 0 ]]; then
      echo $2 $i
      continue
    elif [[ $i -eq 0 ]]; then
        echo $3
        continue
    else 
        echo $2 $i
    fi
  done

}


if [[ -n $HELP ]]; then 
  echo -e "welkome to the help of title generator, with -m {number or nothing} you can generate all the chapters names from 0 to 43 or from {number} to 43\nsame with -t {number or nothing} for generating the titles from 0 to 43 or from {number} to 43\nand you can only use one at a time\n"
  exit 0
fi


if [[ $# -gt 2 || $# -eq 0 || -n $NAMESFROM && -n $TITLESFROM ]]; then
  echo -e "\033[35monly -m {starting by} |or| -t {starting by} plis\033[0m"
  exit 1
fi 

if [[ -n $NAMESFROM ]]; then
  
  DEFAULT=1
  generar "-m" $NAMEBASE "Forleikur"

elif [[ -n $TITLESFROM ]]; then

  DEFAULT=1
  generar "-t" $TITLEBASE "Forleikur"

fi


