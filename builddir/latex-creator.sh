#!/usr/bin/env bash 

FILE=$1
CHAP=$2
TITLE="$3"
NAME="$4"
LNF=$5 
LNT=$6 

FILR="$FILE-$CHAP".tex
TEXTCHAPTER="\chap{${TITLE}}"
POEMCHAPTER="\poemsglbox{${NAME}}{${LNF}}{${LNT}}{"
#echo $TEXTCHAPTER

HELP=$(echo $@ | grep -e "--help" -e "-h" )
echo $HELP
#if help is needed

if [[ ! -z $HELP ]]; then
  echo "FILE=\$1, CHAP=\$2, TITLE=\$3, NAME=\$4, LNF=\$5, LNT=\$6 "
else 
  
  #Start latex creation/formating
  echo "%-----þ KAFLI ${CHAP} þ------\n\n" > $FILR
  
  echo -e "\n" >>  $FILR
  
  #start chapter
  echo $TEXTCHAPTER >> $FILR
  echo -e "\n" >>  $FILR

  if [[ $CHAP -eq 0 ]]; then
    echo -e "\\\\notebox{öll () eru athugasemdir höfundar}\n" >> $FILR
 #   echo -e "\\\\color{black}\n\\\\large\n\\\\begin{tcolorbox}[colback=yellow!10!white,colframe=yellow!50!black,\nevery box/.style={fonttitle=\\\\bfseries},title=Note]\n{öll () eru athugasemdir höfundar}\n\\\\end{tcolorbox}\n" >> $FILR 
  fi


  #start poem
  echo $POEMCHAPTER >> $FILR

  #we put the chapter in place
  sed "${LNF},${LNT}!d" $FILE >> $FILR

  echo -e "\n}" >> $FILR
  echo  "\datename" >> $FILR
  echo "\newpage" >> $FILR
  
  #we end the chapter and print that we are done
  echo -e "\033[1;33mchapter \033[0;32m${CHAP} \033[1;33mis \033[0;32mREADY\033[0m"
fi
