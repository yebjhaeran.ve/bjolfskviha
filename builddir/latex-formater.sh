#!/usr/bin/env bash
#------------arguments

FILE=$1
FILENAME=$2
HELP=$( echo $@ | grep -e "--help" -e "-h" )

#----------FUNTIONS
helpCommand ()
{  
 echo -e "\033[1;32m this script takes the original file of \033[1;34m Pétur Knútsson\033[1;32m\n from the \033[1;33mháskoli íslands \033[1;32m server and format them into latex-compatible chapters \n\n \033[1;31mall latex commands needed for funtioning will be included in the basic.tex file\n\033[1;34m\n arguments:\t FILE=\$1 FILENAME=\$2 \n FILENAME is used for save the result"
 
}

brackets ()
{
  sed 's/\[.*/(&)/g' | sed 's/\[//g'
}

persentage ()
{
   sed 's/\%//g'
}

numbering ()
{
  sed -r 's/(\s+)?\S+//1'
}

correctLineIdentation () {
  sed 's/(/\n(/g' | sed -z 's/)\n  \n(/)\n(/g'
}

verseEnviromentIdentation () {
   sed 's/$/\\\\/g' 
}

voidLines () {

  sed '/^$/d'
}
formating ()
{
  
  cat $FILE | brackets | persentage | numbering | correctLineIdentation | voidLines | verseEnviromentIdentation
}

#----------------WORKING/MAIN FUNTIONALLITY



if [[ ! -z $HELP ]]; then

  helpCommand

elif [[ -z $FILE ]]; then
  
  helpCommand

elif [[ -z $FILENAME ]]; then
  echo -e "\033[1;31m Name of the saving file were not given\n\t saved in:\033[1;32m $FILE-fmt\033[0m"
  formating > $FILE-fmt
#elif [[ -e $FILENAME ]]; then

  
else 
  echo -e "\033[1;33m Name of the saving file were not given\n\t saved in:\033[1;32m $FILENAME\033[0m"
  formating > $FILENAME
fi
