#!/usr/bin/env bash

HELP=$(echo $@ | grep -e "-h" -e "--help")
OPTION=$2


if [[ -n $HELP ]]; then
    echo "this program just puts together all the final book and spits it out into Bjólskviða.tex
    \nthe first argument is the compendium of all the chapters with latex format\nthe second is either --bk or --cl for the black and white version or the color version"
fi

rm Bjólskiða-$OPTION.tex
rm ../Bjólskiða-$OPTION.tex

cat basic-$OPTION.tex >> Bjólskiða-$OPTION.tex
cat intro-$OPTION.tex >> Bjólskiða-$OPTION.tex
cat $1.tex >> Bjólskiða-$OPTION.tex
cat end.tex >> Bjólskiða-$OPTION.tex


