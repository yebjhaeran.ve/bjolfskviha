# Bjólfskviða Íslensku Þýðingunni

## ***Download***

***BK version: [BK pdf (the best)](https://gitlab.com/yebjhaeran.ve/bjolfskviha/-/raw/files/Bj%C3%B3lski%C3%B0a-bk.pdf)***
***Color Version: [CL pdf (very good)](https://gitlab.com/yebjhaeran.ve/bjolfskviha/-/raw/files/Bj%C3%B3lski%C3%B0a-cl.pdf)**

all the source code and pdfs can be found in [***the release page***](https://gitlab.com/yebjhaeran.ve/bjolfskviha/-/releases)

## Motivation
This proyect emerges over 2 years ago back when i was learning icelandic, while searching for interesting reading material i found the works of the doctor Petur Knutson whom translated the whole Beowulf to icelandic in a document disponible directly in his personal page in Haskola Islands wich now has been taken dawn, but still avaliable in wayback machine.

And a lot of value is found in this translation sice the events described in the original Beowulf ocur in scandinavia arround the 700 ac and back then the language spoken by the proto norse (whom lived arround scandinavia and the presumably authors of this work) people was proto norse the "mother tonge" of the old norse language, and we know that for the time over on which we have the original Beowulf text written in old english the old norse language was already being spoken by the vikings

and beeing the icelandic language the closest language we have to old norse and the fact that they are so similiar that any fluent speaker of icelandic is capable of reading old norse, with some small dificulties and changes in vocabulary is very important for this proyect, since as far as the internet goes this is the only complete translation thats avalilable to us for icelandic or old norse.

so feel free to read, comment and change the book to your linking

##Licencing
this whole proyect is double licenced
under the GPLv3 and Creative Commons BY-NC-SA (BY: you need yo give attribution to the authors, NC: Not comertial use is allowed, SA: Must be redistributed under the same terms),
all the shell scripts are GPLv3,
the latex reedition of this creative work is under CC BY-NC-SA so feel free to modify it and share it with others, just put my name as the original editor and Peturs name as the author


## Build instructions from source
first you will need a linux or unix like shell as the one of macos or cygwin in the case of widows, if you are unfammiliar with the terms i strongly suggest to reasearch about the topics *"terminal emulator"* and *"command shell"*

once you are in such an environment you will need the gnu core utils installed or any equivalent like the ones of bsd, aka "sed, cat, bash, echo, rm, mv" which must be disponible by default

and have a working instalation of lualatex\texlive in your system as in the case of arch linux installing texlive is enough
is important to note that in case you install it it's nesesary that you install the complete environment including the extra packages and the icelandic support for babel wich must to be included in the texlive-lang-european or texlive-lang-others package

once you have those prerequisites do your modifications to basic.tex for including tex packages, making or modiffying custom function definitions, defining colors or changing the document geometry, style or class

and modify the intro.tex for changing the frontpage, and the beggining of the document, and lastly you can modify the end.tex for the ending of the book,

all the chapters, and book contents are modified in two main ways 

1) changing the text, the contents, if you want to modify the contents of the book you should look out for the bj-orig that file has all the book contents, if you proceed to modify it, follow the authors original notation, and if you want you can keep intact of the original file, make a copy modify that file and after that process you need to modify the build.sh file adding the name of your copy infront of ./latex-master.sh and that copy most to be inside the builddir folder.

after changing the contents **you can build the book by opening a terminal inside the root of the proyect (where the build.sh file is located) and running build.sh by: *./build.sh***

2) changing the latex format around it, once you are dicided to change the structure you need to modify the builddir/latex-format.sh file and adding/modiffying your own choiches for the format only format of the text, for changing the latex code surounding it you need to modify the builddir/latex-creator.sh file, here you add all you want in latex, then for chaging the name of the chapters you need to modify 2 files lines and or title-generator.sh, on lines are defined the lines that make up each chapter, nothe that it always has to start with 1 and end in 3182 since is the last line, and the name of the chapters are generated by title-generator.sh (technicality: if you make your own chapter names you need to make sure that either you define each one, one by one in builddir/latex-cr-auto.sh changing the variables NAMES and TITLES to NAMES=( list of names), TITLES=(List of titles) or make sure that title-generator.sh "echoes" the names one by one as it currently does )

if you have any doubts about the use of any of the scripts feel free to run them with the --help flag, example: 

> cd builddir
> ./latex-master.sh --help
or 
> ./latex-master.sh -h

